let images = [];
let i = 0;

for(i; i < 3; i++)
{
    images[i] = 'images/slider_preview_' + i + '.png';
}

i = 0;

changeImage();

$(document).on('click', '.slider-btn', function(e) {
    e.preventDefault();
    let button = $(this).data('button');
    changeImage(button);
});

function changeImage(button = null) {
    let $image = $('.slider').find('img');

    if(button) {
        if(button == 'up') {
            if(i < 2) {
                i++;
            } else {
                return false;
            }
        } else if(button == 'down') {
            if(i > 0) {
                i--;
            } else {
                return false;
            }
        }

        $image.fadeOut('slow', function() {
            $image.attr('src', images[i]);
            $image.fadeIn('slow');
        });

        $('.slider-page').text(i + 1);
    } else {
        setTimeout(function() {
            if(i < 2) {
                i++;
            } else {
                i = 0;
            }

            $image.fadeOut('slow', function() {
                $image.attr('src', images[i]);
                $image.fadeIn('slow');
            });

            $('.slider-page').text(i + 1);

            changeImage();
        }, 10000);
    }
}
