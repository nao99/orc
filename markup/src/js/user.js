$(function () {
    var hbs;

    $(document).on('click', '.authorization-area', function (e) {
        e.preventDefault();

        hbs = new HbsModal('home_register_and_auth__modal', {isLogin: 1});
        hbs.init();
    });

    $(document).on('submit', '#auth-form', function (e) {
        e.preventDefault();

        let $form = $(this);
        sendRequest($form)
    });

    $(document).on('click', '.authorization-register', function (e) {
        e.preventDefault();
        hbs.destroy();

        hbs = new HbsModal('home_register_and_auth__modal', {isLogin: 0});
        checkModal(hbs);
    });

    $(document).on('submit', '#register-form', function (e) {
        e.preventDefault();

        let $form = $(this);
        sendRequest($form);
    });

    function sendRequest($form) {
        let $form_class = $('.modal-content').find('form');
        let $input = $form_class.find('input, textarea');
        $input.on('keyup.input-shadow', function () {
            $(this).removeClass('error');
            $(this).siblings('.error-text').remove();
        });

        $form.ajaxSubmit({
            dataType: 'json',
            success: function (response) {
                if (response['success']) {
                    location.href = '/';
                } else {
                    $.each(response['errors'], function (index, item) {
                        let $field = $form.find('[name="' + index + '"]');

                        if (!$field.hasClass('error')) {
                            $field.addClass('error');
                            $field.after('<div class="error-text">' + item + '</div>');
                        }
                    });
                }
            }
        });
    }

    function checkModal(hbs) {
        if ($('.modal').length) {
            setTimeout(function () {
                checkModal(hbs);
            }, 30);
        } else {
            hbs.init();
        }
    }
});