var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var del = require('del');
var path = require('path');
var handlebars = require('gulp-handlebars');
var wrap = require('gulp-wrap');
var declare = require('gulp-declare');

gulp.task('build-css', function() {
    return gulp.src(['src/css/modules/**/*.scss', 'src/css/layout/*.scss'])
        .pipe(sass())
        .pipe(concat('common.css'))
        .pipe(gulp.dest('../public/css'));
});

gulp.task('build-js', function() {
    return gulp.src('src/js/*.js')
        .pipe(concat('common.js'))
        .pipe(gulp.dest('../public/js'));
});

gulp.task('build-images', function() {
    return gulp.src('src/images/**/*.{png,jpg}')
        .pipe(gulp.dest('../public/images'));
});

gulp.task('build-fonts', function() {
    return gulp.src(['src/fonts/**/*.{woff,ttf,woff2,eot}', 'bower_components/mdi/fonts/*.{woff,ttf,woff2,eot}'])
        .pipe(gulp.dest('../public/fonts'));
});

gulp.task('build-handlebars', function() {
    return gulp.src('src/handlebars/**/*.hbs')
        .pipe(handlebars())
        .pipe(wrap('Handlebars.template(<%= contents %>)'))
        .pipe(declare({
            namespace: 'hbs.templates',
            noRedeclare: true
        }))
        .pipe(concat('templates.js'))
        .pipe(gulp.dest('../public/js/'));
});

gulp.task('build-vendor-js', function() {
    return gulp.src(['bower_components/jquery/dist/jquery.js',
        'bower_components/jquery-form/jquery.form.js',
        'bower_components/bootstrap/dist/js/bootstrap.js',
        'bower_components/handlebars/handlebars.js',
        'src/js/core/*.js'])
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest('../public/js'));
});

gulp.task('build-vendor-css', function() {
    return gulp.src(['src/css/helpers/*.scss', 'bower_components/bootstrap/dist/css/bootstrap.css', 'bower_components/mdi/css/materialdesignicons.css'])
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest('../public/css'));
});


gulp.task('watch', function () {
    gulp.watch('src/css/**/*.scss', gulp.series('build-css'));
    gulp.watch('src/js/*.js', gulp.series('build-js'));
    gulp.watch('src/handlebars/**/*.hbs', gulp.series('build-handlebars'));

    var $imagesWatcher = gulp.watch('src/images/**/*.{png,jpg}', gulp.series('build-images'));
    var $fontsWatcher = gulp.watch('src/fonts/**/*.{ttf,woff,woff2,eot,svg}', gulp.series('build-fonts'));

    $imagesWatcher.on('unlink', function(filepath) {
        var filePathFromSrc = path.relative(path.resolve('src'), filepath);
        var destFilePath = path.resolve('../public', filePathFromSrc);

        console.log(destFilePath);

        del.sync(destFilePath, {force: true});
    });

    $fontsWatcher.on('unlink', function(filepath) {
        var filePathFromSrc = path.relative(path.resolve('src'), filepath);
        var destFilePath = path.resolve('../public', filePathFromSrc);

        del.sync(destFilePath, {force: true});
    });
});

gulp.task('external', gulp.series(
    'build-css', 'build-js', 'build-images', 'build-vendor-js',
    'build-vendor-css', 'build-fonts', 'build-handlebars', 'watch'));
