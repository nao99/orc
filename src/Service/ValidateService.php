<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\CategoryRepository;
use App\Repository\ProducerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * This class contains methods for validation anybody data
 */
class ValidateService
{
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * Validate of user data
     *
     * @param array $data
     * @param UserPasswordEncoderInterface $encoder
     *
     * @return array
     */
    public function validateUserLogin(array $data, UserPasswordEncoderInterface $encoder): array
    {
        $errors = [];
        $result = [];

        if ($data['is_logged']) {
            $errors['user_name'] = 'Вы уже были авторизированы';
        } else {
            /**@var User $user */
            $user = $this->em->getRepository('App:User')->findOneBy(['userName' => $data['user_name']]);

            if ($user) {
                if (!$encoder->isPasswordValid($user, $data['password'] . $user->getSalt())) {
                    $errors['password'] = 'Пароль указан неверно';
                } else {
                    $result = [
                        'errors' => null,
                        'user' => $user
                    ];
                }
            } else {
                $errors['user_name'] = 'Пользователя с таким именем не существует';
            }
        }

        if ($errors) {
            $result = [
                'errors' => $errors
            ];
        }

        return $result;
    }

    /**
     * Validate of potential user data
     *
     * @param array $data
     * @return array
     */
    public function validateUserRegister(array $data): array
    {
        $em = $this->em;

        $errors = [];

        if ($data['is_logged']) {
            $errors['user_name'] = 'Вы уже авторизованы';
        } else {
            if (!$data['user_name'] || strlen($data['user_name']) < 5) {
                $errors['user_name'] = 'Имя пользователя не может быть пустым или меньше 5 символов';
            } else {
                if (!preg_match('/^[a-zA-Z][a-zA-Z0-9-_\.]{4,20}$/', $data['user_name'])) {
                    $errors['user_name'] = 'Имя пользователя может содержать только буквенные и числовые символы ';
                } else {
                    $user = $em->getRepository('App:User')->findOneBy(['userName' => $data['user_name']]);

                    if ($user) {
                        $errors['user_name'] = 'Данное имя уже занято';
                    }
                }
            }
        }

        if (!$data['password']) {
            $errors['password'] = 'Пароль не может быть пустым';
        } else {
            if (!preg_match('/^(?=.*[A-Z].*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z].*[a-z]).{8}$/', $data['password'])) {
                $errors['password'] = 'Пароль не является надежным';
            }
        }

        return $errors;
    }

    /**
     * Validate of potential product data
     *
     * @param array $data
     * @return array
     */
    public function validateProduct(array $data): array
    {
        $errors = [];

        foreach ($data as $key => $value) {
            switch ($key) {
                case 'category':

                    if (!$value || !is_int($value)) {
                        $errors['category'] = 'Это не категория';
                    } else {
                        $category = $this->em->getRepository(CategoryRepository::class)->find($value);

                        if (!$category) {
                            $errors['category'] = 'Такой категории не существует';
                        }
                    }

                    break;

                case 'producer':

                    if (!$value || !is_int($value)) {
                        $errors['producer'] = 'Это не производитель';
                    } else {
                        $producer = $this->em->getRepository(ProducerRepository::class)->find($value);

                        if (!$producer) {
                            $errors['producer'] = 'Такого производителя не существует';
                        }
                    }

                    break;

                case 'title':

                    if (!$value) {
                        $errors['title'] = 'Название не может быть пустым';
                    } else {
                        $data['title'] = trim($value);
                    }

                    break;

                case 'description':

                    if ($value) {
                        $data['description'] = trim($value);
                    }

                    break;

                case 'cost':

                    if (!$value) {
                        $errors['cost'] = 'Цена не может быть пустой';
                    } else if (!is_int($value)) {
                        $errors['cost'] = 'Цена не может быть строкой';
                    }

                    break;
            }
        }

        return $errors;
    }
}
