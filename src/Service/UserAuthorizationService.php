<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * This class contains methods for user authorization
 */
class UserAuthorizationService
{
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * Method that authorize user
     *
     * @param User $user
     * @param bool $isRemember
     * @param TokenStorage $tokenStorage
     * @param Session $session
    */
    public function authorize(User $user, bool $isRemember, TokenStorage $tokenStorage, Session $session): void
    {
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());

        $tokenStorage->setToken($token);
        $session->set('_security_main', serialize($token));
    }
}
