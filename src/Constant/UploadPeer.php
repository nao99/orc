<?php

namespace App\Constant;

/**
 * This class contains constants of the upload table
 */
abstract class UploadPeer
{
    const PATH = 'uploads';
}