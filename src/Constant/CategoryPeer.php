<?php

namespace App\Constant;

/**
 * This class contains constants of the category table
*/
abstract class CategoryPeer
{
    const LIQUID    = 1;
    const MAUD      = 2;
    const ATOMIZERS = 3;
    const CHARGER   = 4;
    const PRODUCT   = 5;
}