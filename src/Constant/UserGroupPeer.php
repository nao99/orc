<?php

namespace App\Constant;

/**
 * This class contains constants of the user group table
 */
abstract class UserGroupPeer
{
    const ADMIN  = 1;
    const SELLER = 2;
    const USER   = 3;
}