<?php

namespace App\Controller;

use App\Manager\UserManager;
use App\Service\UserAuthorizationService;
use App\Service\ValidateService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    private $encoder;
    private $validateService;
    private $authorizationService;

    /**
     * @param UserPasswordEncoderInterface $encoder
     * @param ValidateService $validateService
     * @param UserAuthorizationService $authorizationService
    */
    public function __construct(UserPasswordEncoderInterface $encoder, ValidateService $validateService, UserAuthorizationService $authorizationService)
    {
        $this->encoder = $encoder;
        $this->validateService = $validateService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * @Route(path="/register/", name="register", methods={"POST"})
     *
     * @param Request $request
     * @param UserManager $userManager
     *
     * @return JsonResponse
     */
    public function register(Request $request, UserManager $userManager)
    {
        $data = [
            'user_name' => $request->request->get('user_name'),
            'password'  => $request->request->get('password'),
            'is_logged' => $this->get('session')->get('_security_main') ? 1 : 0
        ];

        $errors = $this->validateService->validateUserRegister($data);

        if (!$errors) {
            $data['ip'] = $request->server->get('REMOTE_ADDR');
            $userManager->createUser($data);

            $response = [
                'success' => true,
            ];
        } else {
            $response = [
                'success' => false,
                'errors' => $errors
            ];
        }

        return new JsonResponse($response);
    }

    /**
     * @Route(path="/login/", name="login", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $data = [
            'user_name'   => $request->request->get('user_name'),
            'password'    => $request->request->get('password'),
            'is_remember' => $request->request->get('is_remember'),
            'is_logged'   => $this->get('session')->get('_security_main') ? 1 : 0
        ];

        $result = $this->validateService->validateUserLogin($data, $this->encoder);

        if (!$result['errors']) {
            $session = $this->get('session');
            $tokenStorage = $this->get('security.token_storage');
            $isRemember = $data['is_remember'] ? 1 : 0;

            $this->authorizationService->authorize($result['user'], $isRemember, $tokenStorage, $session);

            $response = [
                'success' => true,
            ];
        } else {
            $response = [
                'success' => false,
                'errors' => $result['errors']
            ];
        }

        return new JsonResponse($response);
    }

    /**
     * @Route(path="/logout/", name="logout")
     *
     * @return object
     */
    public function logout()
    {
        return $this->redirectToRoute('index');
    }

    /**
     * @Route(path="/profile/", name="profile")
     *
     * @return JsonResponse
    */
    public function profile()
    {
        return new JsonResponse();
    }
}

