<?php

namespace App\Controller;

use App\Constant\UploadPeer;
use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route(path="/", name="index")
     *
     * @param ProductRepository $productRepository
     *
     * @return object
     */
    public function index(ProductRepository $productRepository)
    {
        $products = $productRepository->findLastLiquids();

        /**
         * @var $product Product
        */

        $results = [];
        foreach ($products as $product) {
            $sale = $product->getSale();
            $oldCost = $product->getCost();
            $newCost = null;

            $preview = null;
            foreach ($product->getImages()->getValues() as $image) {
                if ($image->getIsPreview()) {
                    $upload = $image->getUpload();
                    $preview = UploadPeer::PATH . DIRECTORY_SEPARATOR . $upload->getPath() . DIRECTORY_SEPARATOR . $upload->getName() . '.' . $upload->getExpansion();
                }
            }

            if ($sale) {
                if ($sale->getIsActive()) {
                    $newCost = $oldCost - $oldCost * $sale->getPercent() / 100;
                }
            }

            $results[] = [
                'preview' => $preview,
                'title'   => $product->getTitle(),
                'sale'    => $sale ? $sale->getPercent() : null,
                'oldCost' => $oldCost,
                'newCost' => $newCost
            ];
        }

        return $this->render('index.html.twig', ['products' => $results]);
    }
}

