<?php

namespace App\Repository;

use App\Entity\InformImage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InformImage|null find($id, $lockMode = null, $lockVersion = null)
 * @method InformImage|null findOneBy(array $criteria, array $orderBy = null)
 * @method InformImage[]    findAll()
 * @method InformImage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InformImageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InformImage::class);
    }

    // /**
    //  * @return InformImage[] Returns an array of InformImage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InformImage
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
