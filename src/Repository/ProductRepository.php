<?php

namespace App\Repository;

use App\Constant\CategoryPeer;
use App\Constant\UploadCategoryPeer;
use App\Entity\Product;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * This class contains methods for select from the product table
 *
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findLastLiquids()
    {
        $qb = $this->createQueryBuilder('product');

        return $qb
            ->innerJoin('App:ProductQuantity', 'productQuantity', Join::WITH, 'productQuantity.product = product.id')
            ->where($qb->expr()->eq('product.category', CategoryPeer::LIQUID))
            ->andWhere($qb->expr()->neq('productQuantity.stock', 0))
            ->orderBy('product.id', Criteria::DESC)
            ->setMaxResults(9)
            ->getQuery()
            ->getResult();
    }
}