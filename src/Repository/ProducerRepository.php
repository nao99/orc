<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * This class contains methods for select from the producer table
 */
class ProducerRepository extends EntityRepository
{
}