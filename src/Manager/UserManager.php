<?php

namespace App\Manager;

use App\Constant\LoyaltyPeer;
use App\Constant\UserGroupPeer;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * This class contains methods for working with the user table
 */
class UserManager
{
    private $em;
    private $encoder;

    /**
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $encoder
    */
    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $encoder)
    {
        $this->em = $entityManager;
        $this->encoder = $encoder;
    }

    /**
     * Create user method
     *
     * @param array $data
    */
    public function createUser(array $data): void
    {
        $group = $this->em->getRepository('App:UserGroup')->findOneBy(['id' => UserGroupPeer::USER]);
        $loyalty = $this->em->getRepository('App:Loyalty')->findOneBy(['id' => LoyaltyPeer::BEGINNER]);
        $salt = substr(base_convert(sha1(uniqid(mt_rand(), true)), 16, 36), 0, 8);

        $user = new User();
        $user
            ->setUsername($data['user_name'])
            ->setSalt($salt)
            ->setLoyalty($loyalty)
            ->setGroup($group)
            ->setIp($data['ip']);

        $encodedPassword = $this->encoder->encodePassword($user, $data['password'] . $salt);
        $user->setPassword($encodedPassword);

        $this->em->persist($user);
        $this->em->flush();
    }
}