<?php

namespace App\Entity;

use App\Constant\UserGroupPeer;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Database table of users.
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="user")
 * @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="ProductComment", mappedBy="user", cascade={"remove"})
     */
    protected $comment;

    /**
     * @ORM\OneToMany(targetEntity="Order", mappedBy="user", cascade={"remove"})
     */
    protected $order;

    /**
     * @ORM\OneToMany(targetEntity="UserCertificate", mappedBy="user", cascade={"remove"})
     */
    protected $certificate;

    /**
     * @ORM\OneToOne(targetEntity="DeliveryInfo", mappedBy="user", cascade={"remove"})
     * @ORM\JoinColumn(name="delivery_info_id", referencedColumnName="id", nullable=true)
     */
    protected $deliveryInfo;

    /**
     * @ORM\ManyToOne(targetEntity="UserGroup")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id", nullable=false)
    */
    protected $group;

    /**
     * @ORM\ManyToOne(targetEntity="Loyalty")
     * @ORM\JoinColumn(name="loyalty_id", referencedColumnName="id", nullable=false)
     */
    protected $loyalty;

    /**
     * @ORM\Column(name="user_name", type="string", length=25, nullable=false, unique=true)
    */
    protected $userName;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $password;

    /**
     * @ORM\Column(type="string", length=8, nullable=false)
     */
    protected $salt;

    /**
     * @ORM\Column(type="string", length=15, nullable=false)
     */
    protected $ip;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=true)
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=true)
     */
    protected $updatedAt;

    /**
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        $roles = [];
        $group = $this->getGroup()->getId();

        switch ($group) {
            case UserGroupPeer::ADMIN:
                $roles = ['ROLE_ADMIN', 'ROLE_USER'];

                break;

            case UserGroupPeer::SELLER:
                $roles = ['ROLE_SELLER', 'ROLE_USER'];

                break;

            case UserGroupPeer::USER:
                $roles = ['ROLE_USER'];

                break;
        }

        return $roles;
    }

    public function eraseCredentials() {}

    /**
     * @return UserGroup
     */
    public function getGroup(): UserGroup
    {
        return $this->group;
    }

    /**
     * @param UserGroup $group
     *
     * @return User
     */
    public function setGroup(UserGroup $group): User
    {
        $this->group = $group;

        return $this;
    }

    /**
     * @return DeliveryInfo
     */
    public function getDeliveryInfo(): DeliveryInfo
    {
        return $this->deliveryInfo;
    }

    /**
     * @param DeliveryInfo $deliveryInfo
     *
     * @return User
     */
    public function setDeliveryInfo(DeliveryInfo $deliveryInfo): User
    {
        $this->deliveryInfo = $deliveryInfo;

        return $this;
    }

    /**
     * @return Loyalty
     */
    public function getLoyalty(): Loyalty
    {
        return $this->loyalty;
    }

    /**
     * @param Loyalty $loyalty
     *
     * @return User
     */
    public function setLoyalty(Loyalty $loyalty): User
    {
        $this->loyalty = $loyalty;

        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->userName;
    }

    /**
     * @param string $userName
     *
     * @return User
     */
    public function setUsername(string $userName): User
    {
        $this->userName = $userName;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getSalt(): string
    {
        return $this->salt;
    }

    /**
     * @param string $salt
     *
     * @return User
     */
    public function setSalt(string $salt): User
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * @return string
     */
    public function getIp(): string
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     *
     * @return User
     */
    public function setIp(string $ip): User
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * @return integer
     */
    public function getCreatedAt(): int
    {
        return $this->createdAt ? $this->createdAt : 0;
    }

    /**
     * @param integer $createdAt
     *
     * @return User
     */
    public function setCreatedAt(int $createdAt): User
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return int
     */
    public function getUpdatedAt(): int
    {
        return $this->updatedAt ? $this->updatedAt : 0;
    }

    /**
     * @param integer $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt(int $updatedAt): User
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function setTimestamps(): void
    {
        if (!$this->getCreatedAt()) {
            $this->setCreatedAt(time());
        }
        $this->setUpdatedAt(time());
    }
}
