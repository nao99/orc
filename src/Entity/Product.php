<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * Database table of products.
 *
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @ORM\Table(name="product")
 */
class Product
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=false)
    */
    protected $category;

    /**
     * @ORM\ManyToOne(targetEntity="ProductProducer")
     * @ORM\JoinColumn(name="producer_id", referencedColumnName="id", nullable=false)
    */
    protected $producer;

    /**
     * @ORM\OneToOne(targetEntity="ProductSale", mappedBy="product", cascade={"remove"})
     */
    protected $sale;

    /**
     * @ORM\OneToMany(targetEntity="ProductComment", mappedBy="product", cascade={"remove"})
     */
    protected $comment;

    /**
     * @ORM\OneToOne(targetEntity="ProductQuantity", mappedBy="product", cascade={"remove"})
     */
    protected $quantity;

    /**
     * @ORM\OneToMany(targetEntity="ProductImage", mappedBy="product")
     */
    protected $images;

    /**
     * @ORM\Column(type="string", length=65, nullable=false)
    */
    protected $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="integer", nullable=false)
    */
    protected $cost;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=true)
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=true)
     */
    protected $updatedAt;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory($category): void
    {
        $this->category = $category;
    }

    /**
     * @return ProductProducer
     */
    public function getProducer()
    {
        return $this->producer;
    }

    /**
     * @param ProductProducer $producer
     */
    public function setProducer($producer): void
    {
        $this->producer = $producer;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return integer
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param integer $cost
     */
    public function setCost($cost): void
    {
        $this->cost = $cost;
    }

    /**
     * @return ProductSale
     */
    public function getSale()
    {
        return $this->sale;
    }

    /**
     * @param ProductSale $sale
     */
    public function setSale($sale): void
    {
        $this->sale = $sale;
    }

    /**
     * @return ProductComment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param ProductComment $comment
     */
    public function setComment($comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return ProductQuantity
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param ProductQuantity $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return PersistentCollection ProductImage
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     *
     * @param mixed
     */
    public function setTimestamps(): void
    {
        if (!$this->getCreatedAt()) {
            $this->setCreatedAt(time());
        }
        $this->setUpdatedAt(time());
    }
}
