<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Database table of delivery info.
 *
 * @ORM\Entity
 * @ORM\Table(name="delivery_info")
 */
class DeliveryInfo
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Delivery")
     * @ORM\JoinColumn(name="delivery_id", referencedColumnName="id", nullable=false)
     */
    protected $delivery;

    /**
     * @ORM\Column(type="string", length=25, nullable=false)
    */
    protected $name;

    /**
     * @ORM\Column(type="string", length=25, nullable=false)
     */
    protected $surname;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    protected $patronymic;

    /**
     * @ORM\Column(type="string", length=65, nullable=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=10, nullable=false)
     */
    protected $phone;

    /**
     * @ORM\Column(type="string", length=25, nullable=false)
     */
    protected $country;

    /**
     * @ORM\Column(type="string", length=25, nullable=false)
     */
    protected $city;

    /**
     * @ORM\Column(type="string", length=25, nullable=false)
     */
    protected $street;

    /**
     * @ORM\Column(type="smallint", nullable=false)
     */
    protected $house;

    /**
     * @ORM\Column(type="smallint", nullable=false)
     */
    protected $apartment;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $wish;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     */
    protected $updatedAt;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Delivery
     */
    public function getDelivery()
    {
        return $this->delivery;
    }

    /**
     * @param Delivery $delivery
     */
    public function setDelivery($delivery): void
    {
        $this->delivery = $delivery;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname($surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getPatronymic()
    {
        return $this->patronymic;
    }

    /**
     * @param string $patronymic
     */
    public function setPatronymic($patronymic): void
    {
        $this->patronymic = $patronymic;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country): void
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city): void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street): void
    {
        $this->street = $street;
    }

    /**
     * @return integer
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * @param integer $house
     */
    public function setHouse($house): void
    {
        $this->house = $house;
    }

    /**
     * @return integer
     */
    public function getApartment()
    {
        return $this->apartment;
    }

    /**
     * @param integer $apartment
     */
    public function setApartment($apartment): void
    {
        $this->apartment = $apartment;
    }

    /**
     * @return string
     */
    public function getWish()
    {
        return $this->wish;
    }

    /**
     * @param string $wish
     */
    public function setWish($wish): void
    {
        $this->wish = $wish;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     *
     * @param mixed
     */
    public function setTimestamps(): void
    {
        if (!$this->getCreatedAt()) {
            $this->setCreatedAt(time());
        }
        $this->setUpdatedAt(time());
    }
}
