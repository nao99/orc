<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Database table of products.
 *
 * @ORM\Entity
 * @ORM\Table(name="product_quantity")
 */
class ProductQuantity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="Product", inversedBy="quantity")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $product;

    /**
     * @ORM\Column(type="integer", nullable=false)
    */
    protected $stock;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct($product): void
    {
        $this->product = $product;
    }

    /**
     * @return integer
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param integer $stock
     */
    public function setStock($stock): void
    {
        $this->stock = $stock;
    }
}