<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Database table of user certificates.
 *
 * @ORM\Entity
 * @ORM\Table(name="user_certificate")
 */
class UserCertificate
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="certificate")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $user;

    /**
     * @ORM\Column(type="string", length=65, nullable=false)
     */
    protected $code;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $sum;

    /**
     * @ORM\Column(name="is_used", type="boolean", nullable=false)
     */
    protected $isUsed;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="complete_at", type="integer", nullable=false)
     */
    protected $completeAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     */
    protected $updatedAt;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code): void
    {
        $this->code = $code;
    }

    /**
     * @return integer
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * @param integer $sum
     */
    public function setSum($sum): void
    {
        $this->sum = $sum;
    }

    /**
     * @return boolean
     */
    public function getIsUsed()
    {
        return $this->isUsed;
    }

    /**
     * @param boolean $isUsed
     */
    public function setIsUsed($isUsed): void
    {
        $this->isUsed = $isUsed;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getCompleteAt()
    {
        return $this->completeAt;
    }

    /**
     * @param mixed $completeAt
     */
    public function setCompleteAt($completeAt): void
    {
        $this->completeAt = $completeAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     *
     * @param mixed
     */
    public function setTimestamps(): void
    {
        if (!$this->getCreatedAt()) {
            $this->setCreatedAt(time());
        }

        if (!$this->getCompleteAt()) {
            $completedDate = time() + 3 * 30 * 24 * 60 * 60;
            $this->setCompleteAt($completedDate);
        }
        $this->setUpdatedAt(time());
    }
}
