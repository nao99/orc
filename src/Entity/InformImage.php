<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Table of inform images.
 *
 * @ORM\Entity(repositoryClass="App\Repository\InformImageRepository")
 */
class InformImage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Inform", inversedBy="image")
     * @ORM\JoinColumn(name="infrom_id", referencedColumnName="id")
     */
    protected $inform;

    /**
     * @ORM\ManyToOne(targetEntity="Upload", inversedBy="informImages")
     * @ORM\JoinColumn(name="upload_id", referencedColumnName="id")
     */
    protected $upload;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Inform
     */
    public function getInform()
    {
        return $this->inform;
    }

    /**
     * @param Inform $inform
     */
    public function setInform($inform): void
    {
        $this->inform = $inform;
    }

    /**
     * @return Upload
     */
    public function getUpload()
    {
        return $this->upload;
    }

    /**
     * @param Upload $upload
     */
    public function setUpload($upload): void
    {
        $this->upload = $upload;
    }
}
