<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Database table of inform assessment.
 *
 * @ORM\Entity
 * @ORM\Table(name="inform_assessment")
 */
class InformAssessment
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $user;

    /**
     * @ORM\OneToOne(targetEntity="Inform", inversedBy="assessment")
     * @ORM\JoinColumn(name="inform_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $inform;

    /**
     * @ORM\Column(type="smallint", nullable=false)
     */
    protected $assessment;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=true)
     */
    protected $createdAt;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return Inform
     */
    public function getInform()
    {
        return $this->inform;
    }

    /**
     * @param Inform $inform
     */
    public function setInform($inform): void
    {
        $this->inform = $inform;
    }

    /**
     * @return integer
     */
    public function getAssessment()
    {
        return $this->assessment;
    }

    /**
     * @param integer $assessment
     */
    public function setAssessment($assessment): void
    {
        $this->assessment = $assessment;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     *
     * @param mixed
     */
    public function setTimestamps(): void
    {
        if (!$this->getCreatedAt()) {
            $this->setCreatedAt(time());
        }
    }
}
