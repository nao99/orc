<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Database table of product images.
 *
 * @ORM\Entity(repositoryClass="App\Repository\ProductImageRepository")
 * @ORM\Table(name="product_image")
 */
class ProductImage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="image")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    protected $product;

    /**
     * @ORM\ManyToOne(targetEntity="Upload", inversedBy="productImages")
     * @ORM\JoinColumn(name="upload_id", referencedColumnName="id")
     */
    protected $upload;

    /**
     * @ORM\Column(name="is_preview", type="boolean", nullable=false, options={"default" : 0})
    */
    protected $isPreview;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct($product): void
    {
        $this->product = $product;
    }

    /**
     * @return Upload
     */
    public function getUpload()
    {
        return $this->upload;
    }

    /**
     * @param Upload $upload
     */
    public function setUpload($upload): void
    {
        $this->upload = $upload;
    }

    /**
     * @return boolean
     */
    public function getIsPreview()
    {
        return $this->isPreview;
    }

    /**
     * @param boolean $isPreview
     */
    public function setIsPreview($isPreview): void
    {
        $this->isPreview = $isPreview;
    }
}
