<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Database table of sales.
 *
 * @ORM\Entity
 * @ORM\Table(name="product_sale")
 */
class ProductSale
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="Product", inversedBy="sale")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $product;

    /**
     * @ORM\Column(type="smallint", nullable=false)
    */
    protected $percent;

    /**
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default" : 1})
    */
    protected $isActive;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=true)
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=true)
     */
    protected $updatedAt;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct($product): void
    {
        $this->product = $product;
    }

    /**
     * @return integer
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * @param integer $percent
     */
    public function setPercent($percent): void
    {
        $this->percent = $percent;
    }

    /**
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param boolean $isActive
     */
    public function setIsActive($isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     *
     * @param mixed
     */
    public function setTimestamps(): void
    {
        if (!$this->getCreatedAt()) {
            $this->setCreatedAt(time());
        }
        $this->setUpdatedAt(time());
    }
}
