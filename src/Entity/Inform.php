<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Database table of articles.
 *
 * @ORM\Entity
 * @ORM\Table(name="inform")
 */
class Inform
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="InformImage", mappedBy="inform")
     */
    protected $images;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="inform")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="InformAssessment", mappedBy="inform", cascade={"remove"})
     */
    protected $assessment;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $title;

    /**
     * @ORM\Column(name="short_story", type="string", length=255, nullable=false)
     */
    protected $shortStory;

    /**
     * @ORM\Column(name="full_story", type="text", nullable=true)
     */
    protected $fullStory;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=true)
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=true)
     */
    protected $updatedAt;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getShortStory()
    {
        return $this->shortStory;
    }

    /**
     * @param string $shortStory
     */
    public function setShortStory($shortStory): void
    {
        $this->shortStory = $shortStory;
    }

    /**
     * @return string
     */
    public function getFullStory()
    {
        return $this->fullStory;
    }

    /**
     * @param string $fullStory
     */
    public function setFullStory($fullStory): void
    {
        $this->fullStory = $fullStory;
    }

    /**
     * @return ArrayCollection InformImage
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @return InformAssessment
     */
    public function getAssessment()
    {
        return $this->assessment;
    }

    /**
     * @param InformAssessment $assessment
     */
    public function setAssessment($assessment): void
    {
        $this->assessment = $assessment;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     *
     * @param mixed
     */
    public function setTimestamps(): void
    {
        if (!$this->getCreatedAt()) {
            $this->setCreatedAt(time());
        }
        $this->setUpdatedAt(time());
    }
}
