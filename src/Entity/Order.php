<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Database table of order.
 *
 * @ORM\Entity
 * @ORM\Table(name="order")
 */
class Order
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="order")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="DeliveryInfo")
     * @ORM\JoinColumn(name="delivery_info", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $deliveryInfo;

    /**
     * @ORM\OneToMany(targetEntity="OrderItem", mappedBy="order", orphanRemoval=true, cascade={"remove"})
     */
    protected $orderItem;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $cost;

    /**
     * @ORM\Column(name="is_paid", type="boolean", nullable=false)
     */
    protected $isPaid;

    /**
     * @ORM\Column(type="string", length=25, nullable=false)
     */
    protected $status;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="complete_at", type="integer", nullable=false)
     */
    protected $completeAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     */
    protected $updatedAt;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return DeliveryInfo
     */
    public function getDeliveryInfo()
    {
        return $this->deliveryInfo;
    }

    /**
     * @param DeliveryInfo $deliveryInfo
     */
    public function setDeliveryInfo($deliveryInfo): void
    {
        $this->deliveryInfo = $deliveryInfo;
    }

    /**
     * @return integer
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param integer $cost
     */
    public function setCost($cost): void
    {
        $this->cost = $cost;
    }

    /**
     * @return boolean
     */
    public function getIsPaid()
    {
        return $this->isPaid;
    }

    /**
     * @param boolean $isPaid
     */
    public function setIsPaid($isPaid): void
    {
        $this->isPaid = $isPaid;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getCompleteAt()
    {
        return $this->completeAt;
    }

    /**
     * @param mixed $completeAt
     */
    public function setCompleteAt($completeAt): void
    {
        $this->completeAt = $completeAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     *
     * @param mixed
     */
    public function setTimestamps(): void
    {
        if (!$this->getCreatedAt()) {
            $this->setCreatedAt(time());
        }
        $this->setUpdatedAt(time());
    }
}
